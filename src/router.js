import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Callbacks from './views/Callbacks.vue';
import Promises from './views/Promises.vue';
import WebWorkers from './views/WebWorkers.vue';

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/callbacks',
      name: 'callbacks',
      component: Callbacks,
    },
    {
      path: '/promises',
      name: 'promises',
      component: Promises,
    },
    {
      path: '/web-workers',
      name: 'webWorkers',
      component: WebWorkers,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
