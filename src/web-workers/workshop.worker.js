
self.onmessage = function(e) {
  const workerParams = e.data;
  console.log(workerParams);
  if (workerParams.aString ) {
    let bString = workerParams.aString;
    while (bString.length < 100000000) {
      bString += 'abc';
      // console.log(bString);
    }
    this.postMessage({ bString: bString });
  }
}
